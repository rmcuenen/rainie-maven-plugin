package cuenen.raymond.java.enhance.classfile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code contant_pool} table implementation for a class.
 */
public final class ConstantPool {

    private static final CpInfo RESERVED = new CpInfo(null, new Object());
    /**
     * The {@code constant_pool} table.
     */
    private final List<CpInfo<?>> cpTable = new ArrayList<>();

    public ConstantPool() {
        cpTable.add(RESERVED); // Index 0 is reserved.
    }

    /**
     * Get the {@code cp_info} structure at the given index of the
     * {@code constant_pool} table.
     *
     * @see CpInfo
     * @param <T> Attribute type.
     * @param index the index into the {@code constant_pool} table.
     * @return the {@code cp_info} structure representing the
     * {@code constant_pool} table entry.
     * @throws IncompatibleTypeError when the {@link CpInfo} at the given index
     * is not compatible with the type parameter.
     */
    @SuppressWarnings("unchecked")
    public <T> CpInfo<T> getConstant(int index) throws IncompatibleTypeError {
        try {
            return (CpInfo<T>) cpTable.get(index);
        } catch (ClassCastException ex) {
            throw new IncompatibleTypeError(ex);
        }
    }

    /**
     * Get the {@code info} value of the {@code cp_info} structure at the given
     * index of the {@code consant_pool} table. The {@code cp_info} structure at
     * the given index is first validated to be of the given constant type.
     *
     * @see CpInfo
     * @see Tag
     * @param <T> Attribute type.
     * @param index the index into the {@code constant_pool} table.
     * @param tag the constant type expected to be.
     * @return the {@code info} value of the {@code constant_pool} table entry.
     * @throws IncompatibleTypeError when the expected constant type is not the
     * one found.
     */
    public <T> T getInfo(int index, Tag tag) throws IncompatibleTypeError {
        CpInfo<T> info = getConstant(index);
        if (info.getTag() != tag) {
            throw new IncompatibleTypeError(tag + " != " + info.getTag());
        }
        return info.getInfo();
    }

    public int appendConstant(CpInfo<?> info) {
        if (!cpTable.contains(info)) {
            cpTable.add(info);
        }
        return cpTable.indexOf(info);
    }

    public void write(DataOutputStream out) throws IOException {
        out.writeShort(cpTable.size());
        for (CpInfo<?> info : cpTable) {
            if (RESERVED.equals(info)) {
                continue;
            }
            info.write(out);
        }
    }

    public void read(DataInputStream in) throws IOException {
        cpTable.clear();
        int n = in.readUnsignedShort();
        for (int i = 1; i < n; i++) {
            CpInfo info = new CpInfo();
            info.read(in);
            cpTable.add(info);
            if (info.getTag() == Tag.CONSTANT_Long // Long takes 2 slots.
                    || info.getTag() == Tag.CONSTANT_Double) { // Double takes 2 slots.
                cpTable.add(RESERVED);
                i++;
            }
        }
    }
}
