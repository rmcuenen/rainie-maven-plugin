package cuenen.raymond.java.enhance.classfile;

import cuenen.raymond.java.enhance.classfile.CpInfo.CpRef;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the {@code ClassFile} structure:
 *
 * <pre>
 * ClassFile {
 *     u4             magic;
 *     u2             minor_version;
 *     u2             major_version;
 *     u2             constant_pool_count;
 *     cp_info        constant_pool[constant_pool_count-1];
 *     u2             access_flags;
 *     u2             this_class;
 *     u2             super_class;
 *     u2             interfaces_count;
 *     u2             interfaces[interfaces_count];
 *     u2             fields_count;
 *     field_info     fields[field_count];
 *     u2             methods_count;
 *     method_info    methods[method_count];
 *     u2             attributes_count;
 *     attribute_info attributes[attributes_count];
 * }
 * </pre>
 */
public class ClassFile {

    private static final int MAGIC = 0xCAFEBABE;
    private final int[] version = new int[2];
    private final ConstantPool constantPool = new ConstantPool();
    private final List<Integer> interfaces = new ArrayList<>();
    private final List<FieldInfo> fields = new ArrayList<>();
    private final List<MethodInfo> methods = new ArrayList<>();
    private final List<AttributeInfo> attributes = new ArrayList<>();
    private int access;
    private int thisClass;
    private int superClass;

    public ClassFile() {
    }

    public String getVersion() {
        return version[0] + "." + version[1];
    }

    public ConstantPool getConstantPool() {
        return constantPool;
    }

    public int getAccessFlags() {
        return access;
    }

    public String getThisClass() {
        CpRef classRef = constantPool.getInfo(thisClass, Tag.CONSTANT_Class);
        return constantPool.getInfo(classRef.getIndex(0), Tag.CONSTANT_Utf8);
    }

    public String getSuperClass() {
        if (superClass == 0) {
            return null;
        }
        CpRef classRef = constantPool.getInfo(superClass, Tag.CONSTANT_Class);
        return constantPool.getInfo(classRef.getIndex(0), Tag.CONSTANT_Utf8);
    }

    public List<String> getInterfaces() {
        List<String> result = new ArrayList<>();
        for (Integer index : interfaces) {
            CpRef classRef = constantPool.getInfo(index, Tag.CONSTANT_Class);
            String name = constantPool.getInfo(classRef.getIndex(0), Tag.CONSTANT_Utf8);
            result.add(name);
        }
        return result;
    }

    public List<FieldInfo> getFields() {
        return fields;
    }

    public List<MethodInfo> getMethods() {
        return methods;
    }

    public List<AttributeInfo> getAttributes() {
        return attributes;
    }

    public void appendAttribute(AttributeInfo attribute) {
        if (!attributes.contains(attribute)) {
            attributes.add(attribute);
        }
    }

    public void write(DataOutputStream out) throws IOException {
        out.writeInt(MAGIC);
        out.writeShort(version[1]);
        out.writeShort(version[0]);
        constantPool.write(out);
        out.writeShort(access);
        out.writeShort(thisClass);
        out.writeShort(superClass);
        out.writeShort(interfaces.size());
        for (Integer index : interfaces) {
            out.writeShort(index);
        }
        out.writeShort(fields.size());
        for (FieldInfo field : fields) {
            field.write(out);
        }
        out.writeShort(methods.size());
        for (MethodInfo method : methods) {
            method.write(out);
        }
        out.writeShort(attributes.size());
        for (AttributeInfo attribute : attributes) {
            attribute.write(out);
        }
    }

    public void read(DataInputStream in) throws IOException {
        int magic = in.readInt();
        if (magic != MAGIC) {
            throw new IOException("bad magic number: " + Integer.toHexString(magic));
        }
        version[1] = in.readUnsignedShort();
        version[0] = in.readUnsignedShort();
        constantPool.read(in);
        access = in.readUnsignedShort();
        thisClass = in.readUnsignedShort();
        superClass = in.readUnsignedShort();
        interfaces.clear();
        int n = in.readUnsignedShort();
        for (int i = 0; i < n; i++) {
            interfaces.add(in.readUnsignedShort());
        }
        fields.clear();
        n = in.readUnsignedShort();
        for (int i = 0; i < n; i++) {
            FieldInfo field = new FieldInfo();
            field.read(in);
            fields.add(field);
        }
        methods.clear();
        n = in.readUnsignedShort();
        for (int i = 0; i < n; i++) {
            MethodInfo method = new MethodInfo();
            method.read(in);
            methods.add(method);
        }
        attributes.clear();
        n = in.readUnsignedShort();
        for (int i = 0; i < n; i++) {
            AttributeInfo attribute = new AttributeInfo();
            attribute.read(in);
            attributes.add(attribute);
        }
    }
}
