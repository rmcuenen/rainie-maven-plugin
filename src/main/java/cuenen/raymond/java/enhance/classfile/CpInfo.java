package cuenen.raymond.java.enhance.classfile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * This class represents the {@code cp_info} structure:
 *
 * <pre>
 * cp_info {
 *     u1 tag;
 *     u1 info[];
 * }
 * </pre>
 *
 * @see Tag
 * @param <V> type representation of the {@code info} array.
 */
public final class CpInfo<V> {

    /**
     * This class is used to store index references into the
     * {@code constant_pool} table of a class. This is a general {@code info}
     * value type of the {@code cp_info} structure for entries that only hold
     * references.
     *
     * @see CpInfo
     */
    public static class CpRef {

        /**
         * The referenced indices.
         */
        private final int[] indices;

        /**
         * Creates a new index reference type.
         *
         * @param indices the indices reference to by this type.
         */
        public CpRef(int... indices) {
            this.indices = indices;
        }

        /**
         * Get the referenced index for the given type.
         *
         * @param type the type of index. This is the position of the reference
         * upon creation.
         * @return the referenced index.
         */
        public int getIndex(int type) {
            return indices[type];
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof CpRef) {
                return Arrays.equals(indices, ((CpRef) obj).indices);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return 41 + Arrays.hashCode(indices);
        }

        @Override
        public String toString() {
            return Arrays.toString(indices);
        }
    }
    /**
     * The Constant Type.
     *
     * @see Tag
     */
    private Tag tag;
    /**
     * Additional Information.
     */
    private V info;

    public CpInfo() {
        // Default constructor
    }

    /**
     * Creates the {@code cp_info} structure with the given constant type and
     * additional information.
     *
     * @see Tag
     * @param tag the constant type.
     * @param info the additional information.
     */
    public CpInfo(Tag tag, V info) {
        this.tag = tag;
        this.info = info;
    }

    /**
     * Get the {@code tag} value of this {@code cp_info} structure.
     *
     * @see Tag
     * @return the constant type.
     */
    public Tag getTag() {
        return tag;
    }

    /**
     * Get the {@code info} value of this {@code cp_info} structure.
     *
     * @return the additional information.
     */
    public V getInfo() {
        return info;
    }

    public void write(DataOutputStream out) throws IOException {
        if (tag == null) {
            return;
        }
        CpRef refs;
        out.writeByte(tag.getTagByte());
        switch (tag) {
            case CONSTANT_Utf8:
                out.writeUTF((String) info);
                break;
            case CONSTANT_Integer:
                out.writeInt((Integer) info);
                break;
            case CONSTANT_Float:
                out.writeFloat((Float) info);
                break;
            case CONSTANT_Long:
                out.writeLong((Long) info);
                break;
            case CONSTANT_Double:
                out.writeDouble((Double) info);
                break;
            case CONSTANT_Class:
            case CONSTANT_String:
            case CONSTANT_MethodType:
                refs = (CpRef) info;
                out.writeShort(refs.getIndex(0));
                break;
            case CONSTANT_Fieldref:
            case CONSTANT_Methodref:
            case CONSTANT_InterfaceMethodref:
            case CONSTANT_NameAndType:
            case CONSTANT_InvokeDynamic:
                refs = (CpRef) info;
                out.writeShort(refs.getIndex(0));
                out.writeShort(refs.getIndex(1));
                break;
            case CONSTANT_MethodHandle:
                refs = (CpRef) info;
                out.writeByte(refs.getIndex(0));
                out.writeShort(refs.getIndex(1));
                break;
            default:
                throw new IOException("invalid constant type: " + tag);
        }
    }

    public void read(DataInputStream in) throws IOException {
        int tagByte = in.readUnsignedByte();
        tag = Tag.fromTagByte(tagByte);
        Object value;
        switch (tag) {
            case CONSTANT_Utf8:
                value = in.readUTF();
                break;
            case CONSTANT_Integer:
                value = in.readInt();
                break;
            case CONSTANT_Float:
                value = in.readFloat();
                break;
            case CONSTANT_Long:
                value = in.readLong();
                break;
            case CONSTANT_Double:
                value = in.readDouble();
                break;
            case CONSTANT_Class:
            case CONSTANT_String:
            case CONSTANT_MethodType:
                value = new CpRef(in.readUnsignedShort());
                break;
            case CONSTANT_Fieldref:
            case CONSTANT_Methodref:
            case CONSTANT_InterfaceMethodref:
            case CONSTANT_NameAndType:
            case CONSTANT_InvokeDynamic:
                value = new CpRef(in.readUnsignedShort(),
                        in.readUnsignedShort());
                break;
            case CONSTANT_MethodHandle:
                value = new CpRef(in.readUnsignedByte(),
                        in.readUnsignedShort());
                break;
            default:
                throw new IOException("invalid constant type: " + tagByte);
        }
        info = (V) value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CpInfo) {
            CpInfo that = (CpInfo) obj;
            return this.tag == null ? that.tag == null : this.tag.equals(that.tag)
                    && this.info == null ? that.info == null : this.info.equals(that.info);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 41 * (41 + (tag == null ? 0 : tag.hashCode())) + (info == null ? 0 : info.hashCode());
    }

    @Override
    public String toString() {
        return info + " (" + tag + ")";
    }
}
