package cuenen.raymond.java.enhance.classfile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class representing the common fields of the field_info and
 * method_info structures.
 */
public abstract class AbstractInfo {

    private final List<AttributeInfo> attributes = new ArrayList<>();
    private int access;
    private int name;
    private int descriptor;

    protected AbstractInfo() {
    }

    public int getAccessFlags() {
        return access;
    }

    public String getName(ConstantPool cp) {
        return cp.getInfo(name, Tag.CONSTANT_Utf8);
    }

    public String getDescriptor(ConstantPool cp) {
        return cp.getInfo(descriptor, Tag.CONSTANT_Utf8);
    }

    public List<AttributeInfo> getAttributes() {
        return attributes;
    }

    public void write(DataOutputStream out) throws IOException {
        out.writeShort(access);
        out.writeShort(name);
        out.writeShort(descriptor);
        out.writeShort(attributes.size());
        for (AttributeInfo attr : attributes) {
            attr.write(out);
        }
    }

    public void read(DataInputStream in) throws IOException {
        access = in.readUnsignedShort();
        name = in.readUnsignedShort();
        descriptor = in.readUnsignedShort();
        attributes.clear();
        int n = in.readUnsignedShort();
        for (int i = 0; i < n; i++) {
            AttributeInfo attr = new AttributeInfo();
            attr.read(in);
            attributes.add(attr);
        }
    }
}
