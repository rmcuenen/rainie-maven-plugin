package cuenen.raymond.java.enhance.classfile;

/**
 * An {@code IncompatibleTypeError} is thrown when a type parameter or expected
 * structure is incompatible with the one found at the requested index of the
 * {@code constant_pool} table of a {@code class} file.
 */
public class IncompatibleTypeError extends Error {

    /**
     * Constructs a {@code IncompatibleTypeError} with no detail message.
     */
    public IncompatibleTypeError() {
        super();
    }

    /**
     * Constructs a {@code IncompatibleTypeError} with the specified detail
     * message.
     *
     * @param message the detail message.
     */
    public IncompatibleTypeError(String message) {
        super(message);
    }

    /**
     * Constructs a new error with the specified cause and a detail message of
     * <tt>(cause==null ? null : cause.toString())</tt> (which typically
     * contains the class and detail message of <tt>cause</tt>). This
     * constructor is useful for errors that are little more than wrappers for
     * other throwables.
     *
     * @param cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method). (A <tt>null</tt> value is permitted, and
     * indicates that the cause is nonexistent or unknown.)
     */
    public IncompatibleTypeError(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new error with the specified detail message and cause.
     * <p>
     * Note that the detail message associated with {@code cause} is <i>not</i>
     * automatically incorporated in this error's detail message.
     *
     * @param message the detail message (which is saved for later retrieval by
     * the {@link #getMessage()} method).
     * @param cause the cause (which is saved for later retrieval by the
     * {@link #getCause()} method). (A <tt>null</tt> value is permitted, and
     * indicates that the cause is nonexistent or unknown.)
     */
    public IncompatibleTypeError(String message, Throwable cause) {
        super(message, cause);
    }
}
