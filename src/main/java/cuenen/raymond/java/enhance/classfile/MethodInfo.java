package cuenen.raymond.java.enhance.classfile;

/**
 * This class represents the {@code method_info} structure:
 *
 * <pre>
 * method_info {
 *     u2             access_flags;
 *     u2             name_index;
 *     u2             descriptor_index;
 *     u2             attributes_count;
 *     attribute_info attributes[attributes_count];
 * }
 * </pre>
 */
public final class MethodInfo extends AbstractInfo {

    public MethodInfo() {
        super();
    }
}
