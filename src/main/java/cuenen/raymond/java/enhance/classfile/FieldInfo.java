package cuenen.raymond.java.enhance.classfile;

/**
 * This class represents the {@code field_info} structure:
 *
 * <pre>
 * field_info {
 *     u2             access_flags;
 *     u2             name_index;
 *     u2             descriptor_index;
 *     u2             attributes_count;
 *     attribute_info attributes[attributes_count];
 * }
 * </pre>
 */
public final class FieldInfo extends AbstractInfo {

    public FieldInfo() {
        super();
    }
}
