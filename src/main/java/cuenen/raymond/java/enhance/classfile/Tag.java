package cuenen.raymond.java.enhance.classfile;

/**
 * The constant pool tags.
 *
 * @see #CONSTANT_Class
 * @see #CONSTANT_Fieldref
 * @see #CONSTANT_Methodref
 * @see #CONSTANT_InterfaceMethodref
 * @see #CONSTANT_String
 * @see #CONSTANT_Integer
 * @see #CONSTANT_Float
 * @see #CONSTANT_Long
 * @see #CONSTANT_Double
 * @see #CONSTANT_NameAndType
 * @see #CONSTANT_Utf8
 * @see #CONSTANT_MethodHandle
 * @see #CONSTANT_MethodType
 * @see #CONSTANT_InvokeDynamic
 */
public enum Tag {

    /**
     * The {@code CONSTANT_Class_info} structure is used to represent a class or
     * an interface:
     *
     * <pre>
     * CONSTANT_Class_info {
     *     u1 tag;
     *     u2 name_index;
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item has the value {@code CONSTANT_Class} (7).</li>
     * <li>The value of the {@code name_index} item must be a valid index into
     * the {@code constant_pool} table. The {@code constant_pool} entry at that
     * index must be a {@code CONSTANT_Utf8_info} structure representing a valid
     * binary class or interface name encoded in internal form.</li>
     * </ul>
     */
    CONSTANT_Class(7),
    /**
     * The {@code CONSTANT_Fieldref_info} structure:
     *
     * <pre>
     * CONSTANT_Fieldref_info {
     *     u1 tag;
     *     u2 class_index;
     *     u2 name_and_type_index;
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item has the value {@code CONSTANT_Fieldref}
     * (9).</li>
     * <li>The value of the {@code class_index} item must be a valid index into
     * the {@code constant_pool} table. The {@code constant_pool} entry at that
     * index must be a {@code CONSTANT_Class_info} structure representing either
     * a class type or an interface type that has the field as a member.</li>
     * <li>The value of the {@code name_and_type_index} item must be a valid
     * index into the {@code constant_pool} table. The {@code constant_pool}
     * entry at that index must be a {@code CONSTANT_NameAndType_info}
     * structure. This {@code constant_pool} entry indicates the name and field
     * descriptor of the field.</li>
     * </ul>
     */
    CONSTANT_Fieldref(9),
    /**
     * The {@code CONSTANT_Methodref_info} structure:
     *
     * <pre>
     * CONSTANT_Methodref_info {
     *     u1 tag;
     *     u2 class_index;
     *     u2 name_and_type_index;
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item has the value {@code CONSTANT_Methodref}
     * (10).</li>
     * <li>The value of the {@code class_index} item must be a valid index into
     * the {@code constant_pool} table. The {@code constant_pool} entry at that
     * index must be a {@code CONSTANT_Class_info} structure representing a
     * class type, not an interface type that has the method as a member.</li>
     * <li>The value of the {@code name_and_type_index} item must be a valid
     * index into the {@code constant_pool} table. The {@code constant_pool}
     * entry at that index must be a {@code CONSTANT_NameAndType_info}
     * structure. This {@code constant_pool} entry indicates the name and method
     * descriptor of the method. If the name of the method begins with a '&lt;'
     * ('&#92;u003c'), then the name must be the special name {@code <init>},
     * representing an instance initialization method. The return type of such a
     * method must be {@code void}.</li>
     * </ul>
     */
    CONSTANT_Methodref(10),
    /**
     * The {@code CONSTANT_InterfaceMethodref_info} structure:
     *
     * <pre>
     * CONSTANT_InterfaceMethodref_info {
     *     u1 tag;
     *     u2 class_index;
     *     u2 name_and_type_index;
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item has the value
     * {@code CONSTANT_InterfaceMethodref} (11).</li>
     * <li>The value of the {@code class_index} item must be a valid index into
     * the {@code constant_pool} table. The {@code constant_pool} entry at that
     * index must be a {@code CONSTANT_Class_info} structure representing an
     * interface type that has the method as a member.</li>
     * <li>The value of the {@code name_and_type_index} item must be a valid
     * index into the {@code constant_pool} table. The {@code constant_pool}
     * entry at that index must be a {@code CONSTANT_NameAndType_info}
     * structure. This {@code constant_pool} entry indicates the name and method
     * descriptor of the method.</li>
     * </ul>
     */
    CONSTANT_InterfaceMethodref(11),
    /**
     * The {@code CONSTANT_String_info} structure is used to represent constant
     * objects of the type {@code String}:
     *
     * <pre>
     * CONSTANT_String_info {
     *     u1 tag;
     *     u2 string_index;
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item of the {@code CONSTANT_String_info} structure
     * has the value {@code CONSTANT_String} (8).</li>
     * <li>The value of the {@code string_index} item must be a valid index into
     * the {@code constant_pool} table. The {@code constant_pool} entry at that
     * index must be a {@code CONSTANT_Utf8_info} structure representing the
     * sequence of Unicode code points to which the {@code String} object is to
     * be initialized.</li>
     * </ul>
     */
    CONSTANT_String(8),
    /**
     * The {@code CONSTANT_Integer_info} structure represents 4-byte numeric
     * ({@code int}) constants:
     *
     * <pre>
     * CONSTANT_Integer_info {
     *     u1 tag;
     *     u4 bytes;
     * }
     * </pre>
     * <ul>
     * <Li>The {@code tag} item of the {@code CONSTANT_Integer_info} structure
     * has the value {@code CONSTANT_Integer} (3).</li>
     * <li>The {@code bytes} item of the {@code CONSTANT_Integer_info} structure
     * represents the value of the {@code int} constant. The bytes of the value
     * are stored in big-endian (high byte first) order.</li>
     * </ul>
     */
    CONSTANT_Integer(3),
    /**
     * The {@code CONSTANT_Float_info} structure represents 4-byte numeric
     * ({@code float}) constants:
     *
     * <pre>
     * CONSTANT_Float_info {
     *     u1 tag;
     *     u4 bytes;
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item of the {@code CONSTANT_Float_info} structure has
     * the value {@code CONSTANT_Float} (4).</li>
     * <li>The {@code bytes} item of the {@code CONSTANT_Float_info} structure
     * represents the value of the {@code float} constant in IEEE 754
     * floating-point single format. The bytes of the single format
     * representation are stored in big-endian (high byte first) order.</li>
     * </ul>
     */
    CONSTANT_Float(4),
    /**
     * The {@code CONSTANT_Long_info} represents 8-byte numeric ({@code long})
     * constants:
     *
     * <pre>
     * CONSTANT_Long_info {
     *     u1 tag;
     *     u4 high_bytes;
     *     u4 low_bytes;
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item of the {@code CONSTANT_Long_info} structure has
     * the value {@code CONSTANT_Long} (5).</li>
     * <li>The unsigned {@code high_bytes} and {@code low_bytes} items of the
     * {@code CONSTANT_Long_info} structure together represent the value of the
     * {@code long} constant
     *
     * <pre>
     * ((long) high_bytes &lt;&lt; 32) + low_bytes
     * </pre>
     *
     * where the bytes of each of {@code high_bytes} and {@code low_bytes} are
     * stored in big-endian (high byte first) order.</li>
     * </ul>
     * <p>
     * NOTE: All 8-byte constants take up two entries in the
     * {@code constant_pool} table of the {@code class} file. If a
     * {@code CONSTANT_Long_info} structure is the item in the
     * {@code constant_pool} table at index <i>n</i>, then the next usable item
     * in the pool is located at index <i>n</i>+2. The {@code constant_pool}
     * index <i>n</i>+1 must be valid but is considered unusable.
     */
    CONSTANT_Long(5),
    /**
     * The {@code CONSTANT_Double_info} represents 8-byte numeric
     * ({@code double}) constants:
     *
     * <pre>
     * CONSTANT_Double_info {
     *     u1 tag;
     *     u4 high_bytes;
     *     u4 low_bytes;
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item of the {@code CONSTANT_Double_info} structure
     * has the value {@code CONSTANT_Double} (6).</li>
     * <li>The {@code high_bytes} and {@code low_bytes} items of the
     * {@code CONSTANT_Double_info} structure together represent the
     * {@code double} value in IEEE 754 floating-point double format. The bytes
     * of each item are stored in big-endian (high byte first) order.</li>
     * </ul>
     * <p>
     * NOTE: All 8-byte constants take up two entries in the
     * {@code constant_pool} table of the {@code class} file. If a
     * {@code CONSTANT_Double_info} structure is the item in the
     * {@code constant_pool} table at index <i>n</i>, then the next usable item
     * in the pool is located at index <i>n</i>+2. The {@code constant_pool}
     * index <i>n</i>+1 must be valid but is considered unusable.
     */
    CONSTANT_Double(6),
    /**
     * The {@code CONSTANT_NameAndType_info} structure is used to represent a
     * field or method, without indicating which class or interface type it
     * belongs to:
     *
     * <pre>
     * CONSTANT_NameAndType_info {
     *     u1 tag;
     *     u2 name_index;
     *     u2 descriptor_index;
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item of the {@code CONSTANT_NameAndType_info}
     * structure has the value {@code CONSTANT_NameAndType} (12).</li>
     * <li>The value of the {@code name_index} item must be a valid index into
     * the {@code constant_pool} table. The {@code constant_pool} entry at that
     * index must be a {@code CONSTANT_Utf8_info} structure representing either
     * the special method name {@code <init>} or a valid unqualified name
     * denoting a field or method.</li>
     * <li>The value of the {@code descriptor_index} item must be a valid index
     * into the {@code constant_pool} table. The {@code constant_pool} entry at
     * that index must be a {@code CONSTANT_Utf8_info} structure representing a
     * valid field descriptor or method descriptor.</li>
     * </ul>
     */
    CONSTANT_NameAndType(12),
    /**
     * The {@code CONSTANT_Utf8_info} structure is used to represent constant
     * string values:
     *
     * <pre>
     * CONSTANT_Utf8_info {
     *     u1 tag;
     *     u2 length;
     *     u1 bytes[length];
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item of the {@code CONSTANT_Utf8_info} structure has
     * the value {@code CONSTANT_Utf8} (1).</li>
     * <li>The value of the {@code length} item gives the number of bytes in the
     * {@code bytes} array (not the length of the resulting string). The strings
     * in the {@code CONSTANT_Utf8_info} structure are not null-terminated.</li>
     * <li>The {@code bytes} array contains the bytes of the string. No byte may
     * have the value {@code (byte)0} or lie in the range
     * {@code (byte)0xf0 - (byte)0xff}.</li>
     * </ul>
     * String content is encoded in modified UTF-8. Modified UTF-8 strings are
     * encoded so that code point sequences that contain only non-null ASCII
     * characters can be represented using only 1 byte per code point, but all
     * code points in the Unicode codespace can be represented.
     */
    CONSTANT_Utf8(1),
    /**
     * The {@code CONSTANT_MethodHandle_info} structure is used to represent a
     * method handle:
     *
     * <pre>
     * CONSTANT_MethodHandle_info {
     *     u1 tag;
     *     u1 reference_kind;
     *     u2 reference_index;
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item of the {@code CONSTANT_MethodHandle_info}
     * structure has the value {@code CONSTANT_MethodHandle} (15).</li>
     * <li>The value of the {@code reference_kind} item must be in the range 1
     * to 9. The value denotes the <i>kind</i> of this method handle, which
     * characterizes its bytecode behavior.
     * </li>
     * <li>The value of the {@code reference_index} item must be a valid index
     * into the {@code constant_pool} table.
     * <p>
     * If the value of the {@code reference_kind} item is 1
     * ({@code REF_getField}), 2 ( {@code REF_getStatic}), 3
     * ({@code REF_putField}), or 4 ({@code REF_putStatic}), then the
     * {@code constant_pool} entry at that index must be a
     * {@code CONSTANT_Fieldref_info} structure representing a field for which a
     * method handle is to be created.
     * <p>
     * If the value of the {@code reference_kind} item is 5
     * ({@code REF_invokeVirtual}), 6 ( {@code REF_invokeStatic}), 7
     * ({@code REF_invokeSpecial}), or 8 ( {@code REF_newInvokeSpecial}), then
     * the {@code constant_pool} entry at that index must be a
     * {@code CONSTANT_Methodref_info} structure representing a class's method
     * or constructor for which a method handle is to be created.
     * <p>
     * If the value of the {@code reference_kind} item is 9
     * ({@code REF_invokeInterface}), then the {@code constant_pool} entry at
     * that index must be a {@code CONSTANT_InterfaceMethodref_info} structure
     * representing an interface's method for which a method handle is to be
     * created.
     * <p>
     * If the value of the {@code reference_kind} item is 5
     * ({@code REF_invokeVirtual}), 6 ( {@code REF_invokeStatic}), 7
     * ({@code REF_invokeSpecial}), or 9 ( {@code REF_invokeInterface}), the
     * name of the method represented by a {@code CONSTANT_Methodref_info}
     * structure must not be {@code <init>} or {@code <clinit>}.
     * <p>
     * If the value is 8 ({@code REF_newInvokeSpecial}), the name of the method
     * represented by a {@code CONSTANT_Methodref_info} structure must be
     * {@code <init>}.</li>
     * </ul>
     */
    CONSTANT_MethodHandle(15),
    /**
     * The {@code CONSTANT_MethodType_info} structure is used to represent a
     * method type:
     *
     * <pre>
     * CONSTANT_MethodType_info {
     *     u1 tag;
     *     u2 descriptor_index;
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item of the {@code CONSTANT_MethodType_info}
     * structure has the value {@code CONSTANT_MethodType} (16).</li>
     * <li>The value of the {@code descriptor_index} item must be a valid index
     * into the {@code constant_pool} table. The {@code constant_pool} entry at
     * that index must be a {@code CONSTANT_Utf8_info} structure representing a
     * method descriptor.</li>
     *
     * </ul>
     */
    CONSTANT_MethodType(16),
    /**
     * The {@code CONSTANT_InvokeDynamic_info} structure is used by an
     * <i>invokedynamic</i>
     * instruction to specify a bootstrap method, the dynamic invocation name,
     * the argument and return types of the call, and optionally, a sequence of
     * additional constants called
     * <i>static arguments</i> to the bootstrap method.
     *
     * <pre>
     * CONSTANT_InvokeDynamic_info {
     *     u1 tag;
     *     u2 bootstrap_method_attr_index;
     *     u2 name_and_type_index;
     * }
     * </pre>
     * <ul>
     * <li>The {@code tag} item of the {@code CONSTANT_InvokeDynamic_info}
     * structure has the value {@code CONSTANT_InvokeDynamic} (18).</li>
     * <li>The value of the {@code bootstrap_method_attr_index} item must be a
     * valid index into the {@code bootstrap_methods} array of the bootstrap
     * method table of this {@code class} file.</li>
     * <li>The value of the {@code name_and_type_index} item must be a valid
     * index into the {@code constant_pool} table. The {@code constant_pool}
     * entry at that index must be a {@code CONSTANT_NameAndType_info} structure
     * representing a method name and method descriptor.</li>
     * </ul>
     */
    CONSTANT_InvokeDynamic(18);
    /**
     * The {@code tag} byte value.
     */
    private final int tag;

    /**
     * Creates the constant pool tag with the given byte value.
     *
     * @param tag the byte value.
     */
    private Tag(int tag) {
        this.tag = tag;
    }

    public int getTagByte() {
        return tag;
    }

    /**
     * Retrieve the constant pool tag by its byte value.
     *
     * @param tag the byte value.
     * @return the constant pool tag.
     * @throws IllegalArgumentException for any unknown tag.
     */
    public static Tag fromTagByte(int tag) {
        for (Tag c : Tag.values()) {
            if (c.tag == tag) {
                return c;
            }
        }
        throw new IllegalArgumentException("unknown tag: " + tag);
    }
}
