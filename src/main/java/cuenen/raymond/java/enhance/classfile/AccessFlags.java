package cuenen.raymond.java.enhance.classfile;

public final class AccessFlags {

    public static final int ACC_PUBLIC = 0x0001;
    public static final int ACC_PRIVATE = 0x0002;
    public static final int ACC_PROTECTED = 0x0004;
    public static final int ACC_STATIC = 0x0008;
    public static final int ACC_FINAL = 0x0010;
    public static final int ACC_VOLATILE = 0x0040;
    public static final int ACC_TRANSIENT = 0x0080;
    public static final int ACC_SYNTHETIC = 0x1000;
    public static final int ACC_ENUM = 0x4000;

    public static boolean isPublic(int flags) {
        return (flags & ACC_PUBLIC) == ACC_PUBLIC;
    }

    public static boolean isPrivate(int flags) {
        return (flags & ACC_PRIVATE) == ACC_PRIVATE;
    }

    public static boolean isProtected(int flags) {
        return (flags & ACC_PROTECTED) == ACC_PROTECTED;
    }

    public static boolean isStatic(int flags) {
        return (flags & ACC_STATIC) == ACC_STATIC;
    }

    public static boolean isFinal(int flags) {
        return (flags & ACC_FINAL) == ACC_FINAL;
    }

    public static boolean isVolatile(int flags) {
        return (flags & ACC_VOLATILE) == ACC_VOLATILE;
    }

    public static boolean isTransent(int flags) {
        return (flags & ACC_TRANSIENT) == ACC_TRANSIENT;
    }

    public static boolean isSynthetic(int flags) {
        return (flags & ACC_SYNTHETIC) == ACC_SYNTHETIC;
    }

    public static boolean isEnum(int flags) {
        return (flags & ACC_ENUM) == ACC_ENUM;
    }

    private AccessFlags() {
        // Constants
    }
}
