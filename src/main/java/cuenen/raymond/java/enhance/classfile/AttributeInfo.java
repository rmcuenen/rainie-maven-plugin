package cuenen.raymond.java.enhance.classfile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * This class is used as a base class for the {@code attribute_info} structure.
 * <p>
 * All attributes have the following general format:
 *
 * <pre>
 * attribute_info {
 *     u2 attribute_name_index;
 *     u4 attribute_length;
 *     u1 info[attribute_length];
 * }
 * </pre>
 *
 * <ul>
 * <li>For all attributes, the {@code attribute_name_index} must be a valid
 * unsigned 16- bit index into the constant pool of the class. The
 * {@code constant_pool} entry at {@code attribute_name_index} must be a
 * {@code CONSTANT_Utf8_info} structure representing the name of the
 * attribute.</li>
 * <li>The value of the {@code attribute_length} item indicates the length of
 * the subsequent information in bytes. The length does not include the initial
 * six bytes that contain the {@code attribute_name_index} and
 * {@code attribute_length} items.</li>
 * </ul>
 */
public class AttributeInfo {

    /**
     * The name of the attribute.
     */
    private int name;
    private byte[] info;

    public AttributeInfo() {
        this(-1);
    }

    protected AttributeInfo(int name) {
        this.name = name;
    }

    /**
     * Get the name of the attribute.
     *
     * @param cp The ConstantPool containing the attribute.
     * @return the name of the attribute.
     */
    public String getName(ConstantPool cp) {
        return cp.getInfo(name, Tag.CONSTANT_Utf8);
    }

    public void setInfo(byte[] info) {
        this.info = info;
    }

    public void write(DataOutputStream out) throws IOException {
        out.writeShort(name);
        out.writeInt(info.length);
        out.write(info);
    }

    public void read(DataInputStream in) throws IOException {
        name = in.readUnsignedShort();
        int n = in.readInt();
        info = new byte[n];
        in.readFully(info);
    }

    @Override
    public int hashCode() {
        return 41 * (41 + this.name) + Arrays.hashCode(this.info);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AttributeInfo) {
            AttributeInfo that = (AttributeInfo) obj;
            return this.name == that.name
                    && Arrays.equals(this.info, that.info);
        }
        return false;
    }
}
