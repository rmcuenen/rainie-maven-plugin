package cuenen.raymond.java.enhance;

import cuenen.raymond.java.enhance.classfile.AttributeInfo;
import cuenen.raymond.java.enhance.classfile.ClassFile;
import cuenen.raymond.java.enhance.classfile.ConstantPool;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;
import org.apache.maven.plugin.logging.Log;

public final class RainieEnhancer {

    private final Log logger;

    public RainieEnhancer(Log logger) {
        this.logger = logger;
    }

    public void run(List<File> files) {
        for (File file : files) {
            ClassFile classFile = findClassFile(file);
            if (classFile != null) {
                enhanceClassFile(classFile);
            }
            writeClassFile(file, classFile);
        }
    }

    private ClassFile findClassFile(File file) {
        ClassFile classFile = null;
        DataInputStream in = null;
        try {
            in = new DataInputStream(
                    new FileInputStream(file));
            classFile = new ClassFile();
            classFile.read(in);
        } catch (IOException ex) {
            logger.warn("Failed to read " + file);
        } finally {
            close(in);
        }
        return classFile;
    }

    private void enhanceClassFile(ClassFile classFile) {
        ConstantPool constantPool = classFile.getConstantPool();
        AttributeInfo rainieAttribute = null;
        for (Iterator<AttributeInfo> iter = classFile.getAttributes().iterator();
                iter.hasNext();) {
            AttributeInfo attribute = iter.next();
            if (RainieAttribute.ATTRIBUTE_NAME.equals(
                    attribute.getName(constantPool))) {
                logger.info(classFile.getThisClass() + " already enhanched");
                rainieAttribute = attribute;
                iter.remove();
            }
        }
        if (rainieAttribute == null) {
            logger.info("Enhancing " + classFile.getThisClass());
            rainieAttribute = new RainieAttribute(constantPool);
        }
        classFile.appendAttribute(rainieAttribute);
    }

    private void writeClassFile(File file, ClassFile classFile) {
        DataOutputStream out = null;
        try {
            out = new DataOutputStream(
                    new PrintStream(new FileOutputStream(file), true));
            classFile.write(out);
        } catch (IOException ex) {
            logger.error("Failed to write file " + file, ex);
        } finally {
            close(out);
        }
    }

    /**
     * Silently close the given {@link Closeable}.
     * 
     * @param in
     *            the {@link Closeable} to be closed.
     */
    private void close(Closeable in) {
        if (in != null) {
            try {
                in.close();
            } catch (IOException ex) {
                // Silently ignore.
            }
        }
    }
}
