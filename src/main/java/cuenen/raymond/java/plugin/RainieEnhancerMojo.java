package cuenen.raymond.java.plugin;

import cuenen.raymond.java.enhance.RainieEnhancer;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.codehaus.plexus.util.FileUtils;

@Mojo(name = "enhance",
        defaultPhase = LifecyclePhase.PROCESS_CLASSES,
        requiresDependencyResolution = ResolutionScope.COMPILE)
public class RainieEnhancerMojo extends AbstractMojo {

    @Parameter(property = "rainie.classes", defaultValue = "${project.build.outputDirectory}")
    private File classes;
    @Parameter(defaultValue = "**/*.class")
    private String includes;
    @Parameter(defaultValue = "")
    private String excludes;

    @Override
    public void execute() throws MojoExecutionException {
        if (!getRainieClasses().exists()) {
            FileUtils.mkdir(getRainieClasses().getAbsolutePath());
        }
        List<File> entities = findRainieClassFiles();
        enhance(entities);
    }

    protected File getRainieClasses() {
        return classes;
    }

    protected List<File> findRainieClassFiles() throws MojoExecutionException {
        List<File> files = new ArrayList<>();
        try {
            files = FileUtils.getFiles(getRainieClasses(), includes, excludes);
        } catch (IOException ex) {
            throw new MojoExecutionException("Error while scanning for '" + includes + "' in '"
                    + getRainieClasses().getAbsolutePath() + "'.", ex);
        }
        return files;
    }

    private void enhance(List<File> files) throws MojoExecutionException {
        RainieEnhancer enhancer = new RainieEnhancer(getLog());
        enhancer.run(files);
    }
}
